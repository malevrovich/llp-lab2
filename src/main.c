#include "ast_api.h"

#include <stdio.h>

int main() {
  struct AST* ast = ast_parse();

  FILE* file = fopen("graph.dot", "w+");
  if (file) {
    ast_graphviz(ast, file);
  } else {
    fprintf(stderr, "Unable to open file graph.dot");
  }
  ast_print(ast, stdout);
  ast_free(ast);
  return 0;
}